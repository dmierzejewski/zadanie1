# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
temp = socket.socket(socket.AF_INET,socket.SOCK_STREAM,socket.IPPROTO_IP)
# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31014)  # TODO: zmienić port!
temp.connect(server_address)

try:
    # Wysłanie danych
    message = 'To jest wiadomosc, ktora zostanie zwrocona.'
    temp.send(message.encode('utf-8'))

    # Wypisanie odpowiedzi
    response = temp.recv(4096)
    print(response)

finally:
    temp.close()
    # Zamknięcie połączenia
    pass
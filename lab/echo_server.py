# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
temp = socket.socket(socket.AF_INET,socket.SOCK_STREAM,socket.IPPROTO_IP)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31014)  # TODO: zmienić port!
temp.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
temp.listen(1)
while True:
    # Czekanie na połączenie
    connection, client_address = temp.accept()
    try:
        # Odebranie danych i odesłanie ich spowrotem
        connection.sendall(str(connection.recv(1024)))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
